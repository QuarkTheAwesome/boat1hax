#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include "dynamic_libs/os_functions.h"
#include "dynamic_libs/fs_functions.h"
#include "dynamic_libs/gx2_functions.h"
#include "dynamic_libs/sys_functions.h"
#include "dynamic_libs/vpad_functions.h"
#include "dynamic_libs/padscore_functions.h"
#include "dynamic_libs/socket_functions.h"
#include "dynamic_libs/ax_functions.h"
#include "fs/fs_utils.h"
#include "fs/sd_fat_devoptab.h"
#include "system/memory.h"
#include "utils/logger.h"
#include "utils/utils.h"
#include "common/common.h"
#include "utils/exception.h"

typedef struct _ax_buffer_t {
    u16 format;
    u16 loop;
    u32 loop_offset;
    u32 end_pos;
    u32 cur_pos;
    const unsigned char *samples;
} ax_buffer_t;

unsigned char *screenBuffer;
int screen_buf0_size = 0;
int screen_buf1_size = 0;

#define X_OFFSET 185
#define Y_OFFSET 60

void (*OSScreenPutPixelEx)(int buf, int x, int y, unsigned int colour);
void pix_print(int x, int y, unsigned int colour, int scale) {
	for (int rx = x * scale; rx < ((x * scale) + scale); rx++) {
		for (int ry = y * scale; ry < ((y * scale) + scale); ry++) {
			OSScreenPutPixelEx(0, rx + X_OFFSET, ry + Y_OFFSET, colour);
			OSScreenPutPixelEx(1, rx + X_OFFSET, ry + Y_OFFSET, colour);
		}
	}
}

/* Entry point */
int Menu_Main(void)
{
    //!*******************************************************************
    //!                   Initialize function pointers                   *
    //!*******************************************************************
    //! do OS (for acquire) and sockets first so we got logging
    InitOSFunctionPointers();
    InitSocketFunctionPointers();

	InstallExceptionHandler();

    log_init("192.168.192.36");
    log_print("Starting launcher\n");

    InitFSFunctionPointers();
    InitVPadFunctionPointers();
	InitAXFunctionPointers();

    log_print("Function exports loaded\n");

    //!*******************************************************************
    //!                    Initialize heap memory                        *
    //!*******************************************************************
    log_print("Initialize memory management\n");
    //! We don't need bucket and MEM1 memory so no need to initialize
    memoryInitialize();

    //!*******************************************************************
    //!                        Initialize FS                             *
    //!*******************************************************************
    log_printf("Mount SD partition\n");
    mount_sd_fat("sd");

    VPADInit();

    // Init screen and screen buffers
    OSScreenInit();
    screen_buf0_size = OSScreenGetBufferSizeEx(0);
    screen_buf1_size = OSScreenGetBufferSizeEx(1);

    screenBuffer = MEM1_alloc(screen_buf0_size + screen_buf1_size, 0x100);

    OSScreenSetBufferEx(0, screenBuffer);
    OSScreenSetBufferEx(1, (screenBuffer + screen_buf0_size));

    OSScreenEnableEx(0, 1);
    OSScreenEnableEx(1, 1);

	FILE* fd = fopen("sd:/wiiu/apps/boot1hax/stage2-V.bin", "rb");

	if (!fd) {
		OSFatal("couldn't open stage2-V.bin!");
	}

	fseek(fd, 0L, SEEK_END);
	unsigned int video_size = ftell(fd);
	fseek(fd, 0L, SEEK_SET);

	unsigned int* video = malloc(video_size);

	OSScreenClearBufferEx(0, 0xFF0000FF);
	OSScreenClearBufferEx(1, 0xFF0000FF);

	OSScreenPutFontEx(0, 0, 0, "welcome to boot1hax");
	OSScreenPutFontEx(1, 0, 0, "welcome to boot1hax");
	OSScreenPutFontEx(0, 0, 1, "dumping...");
	OSScreenPutFontEx(1, 0, 1, "dumping...");

	DCFlushRange(screenBuffer, screen_buf0_size + screen_buf1_size);
	OSScreenFlipBuffersEx(0);
	OSScreenFlipBuffersEx(1);

	fread(video, video_size, 1, fd);

	fclose(fd);

	sleep(5);

	OSDynLoad_FindExport(coreinit_handle, 0, "OSScreenPutPixelEx", &OSScreenPutPixelEx);

	/*******************************************************************8
	Audio
	*/
	unsigned int params[3] = {1, 0, 0};
	AXInitWithParams(params);

	void* voice = AXAcquireVoice(25, 0, 0);
	if (!voice) {
		OSFatal("OH DEAR GOD NO");
	}

	FILE* fp = fopen("sd:/wiiu/apps/boot1hax/stage1.bin", "rb");
	if (!fp) {
		OSFatal("couldn't open stage1.bin!");
	}
	fseek(fd, 0L, SEEK_END);
	unsigned int audio_size = ftell(fd);
	fseek(fd, 0L, SEEK_SET);
	unsigned char* audio = malloc(audio_size);

	OSScreenClearBufferEx(0, 0xFF0000FF);
	OSScreenClearBufferEx(1, 0xFF0000FF);

	OSScreenPutFontEx(0, 0, 0, "welcome to boot1hax");
	OSScreenPutFontEx(1, 0, 0, "welcome to boot1hax");
	OSScreenPutFontEx(0, 0, 1, "dumping... dQw4w9WgXcQ");
	OSScreenPutFontEx(1, 0, 1, "dumping... dQw4w9WgXcQ");
	OSScreenPutFontEx(0, 0, 2, "applying patches, please wait...");
	OSScreenPutFontEx(1, 0, 2, "applying patches, please wait...");

	DCFlushRange(screenBuffer, screen_buf0_size + screen_buf1_size);
	OSScreenFlipBuffersEx(0);
	OSScreenFlipBuffersEx(1);

	fread(audio, audio_size, 1, fp);
	fclose(fp);

	sleep(3);

	AXVoiceBegin(voice);
	AXSetVoiceType(voice, 0);
	unsigned int vol = 0x80000000;
	AXSetVoiceVe(voice, &vol);
	unsigned int mix[24];
	memset(mix, 0, sizeof(mix));
	mix[0] = vol;
	mix[4] = vol;
	AXSetVoiceDeviceMix(voice, 0, 0, mix);
	AXSetVoiceDeviceMix(voice, 1, 0, mix);
	AXVoiceEnd(voice);

	ax_buffer_t vBuf;
	memset(&vBuf, 0, sizeof(ax_buffer_t));
	vBuf.samples = audio;
	vBuf.format = 25;
	vBuf.loop = 0;
	vBuf.cur_pos = 0;
	vBuf.end_pos = audio_size - 1;
	vBuf.loop_offset = 0;

	unsigned int ratioBits[4];
	ratioBits[0] = (unsigned int)(0x00010000 * ((float)48000 / (float)AXGetInputSamplesPerSec()));
	ratioBits[1] = 0;
	ratioBits[2] = 0;
	ratioBits[3] = 0;

	AXSetVoiceOffsets(voice, &vBuf);
	AXSetVoiceSrc(voice, ratioBits);
	AXSetVoiceSrcType(voice, 1);

	OSScreenClearBufferEx(0, 0xFF0000FF);
	OSScreenClearBufferEx(1, 0xFF0000FF);

	OSScreenPutFontEx(0, 0, 0, "welcome to boot1hax");
	OSScreenPutFontEx(1, 0, 0, "welcome to boot1hax");
	OSScreenPutFontEx(0, 0, 1, "dumping... dQw4w9WgXcQ");
	OSScreenPutFontEx(1, 0, 1, "dumping... dQw4w9WgXcQ");
	OSScreenPutFontEx(0, 0, 2, "applying patches, please wait...");
	OSScreenPutFontEx(1, 0, 2, "applying patches, please wait...");
	OSScreenPutFontEx(0, 0, 3, "injecting coldboot hook...");
	OSScreenPutFontEx(1, 0, 3, "injecting coldboot hook...");

	DCFlushRange(screenBuffer, screen_buf0_size + screen_buf1_size);
	OSScreenFlipBuffersEx(0);
	OSScreenFlipBuffersEx(1);

	sleep(7);

	OSScreenClearBufferEx(0, 0xFF0000FF);
	OSScreenClearBufferEx(1, 0xFF0000FF);

	OSScreenPutFontEx(0, 0, 0, "welcome to boot1hax");
	OSScreenPutFontEx(1, 0, 0, "welcome to boot1hax");
	OSScreenPutFontEx(0, 0, 1, "dumping... dQw4w9WgXcQ");
	OSScreenPutFontEx(1, 0, 1, "dumping... dQw4w9WgXcQ");
	OSScreenPutFontEx(0, 0, 2, "applying patches, please wait...");
	OSScreenPutFontEx(1, 0, 2, "applying patches, please wait...");
	OSScreenPutFontEx(0, 0, 3, "injecting coldboot hook...");
	OSScreenPutFontEx(1, 0, 3, "injecting coldboot hook...");
	OSScreenPutFontEx(0, 0, 4, "done, restarting into boot1FW...");
	OSScreenPutFontEx(1, 0, 4, "done, restarting into boot1FW...");

	DCFlushRange(screenBuffer, screen_buf0_size + screen_buf1_size);
	OSScreenFlipBuffersEx(0);
	OSScreenFlipBuffersEx(1);

	sleep(3);

	OSScreenClearBufferEx(0, 0);
	OSScreenClearBufferEx(1, 0);
	DCFlushRange(screenBuffer, screen_buf0_size + screen_buf1_size);
	OSScreenFlipBuffersEx(0);
	OSScreenFlipBuffersEx(1);
	OSScreenClearBufferEx(0, 0);
	OSScreenClearBufferEx(1, 0);

	for (unsigned int frame_offset = 0; frame_offset < (video_size / 4); frame_offset += (60*45)) {
		//print frame
		int x = 0;
		int y = 0;
		for (int i = 0; i < (60*45); i++) {
			unsigned int new_colour = ((video[frame_offset + i] & 0xFF000000) >> 16) | ((video[frame_offset + i] & 0x0000FF00) << 16) | ((video[frame_offset + i] & 0x00FF00FF));
			pix_print(x, y, new_colour, 8);
			x++;
			if (x == 60) {
				y++;
				x = 0;
			}
		}

		DCFlushRange(screenBuffer, screen_buf0_size + screen_buf1_size);
		OSScreenFlipBuffersEx(0);
		OSScreenFlipBuffersEx(1);

		int vpadError = -1;
	    VPADData vpad;
	    VPADRead(0, &vpad, 1, &vpadError);

	    if(vpadError == 0 && ((vpad.btns_d) & VPAD_BUTTON_TV)) {
			break;
		}

		usleep(480000); //TODO tweak this value to maintain sync
		if (frame_offset == (60*45)) {
			AXSetVoiceState(voice, 1);
		}
	}
	AXSetVoiceState(voice, 0);

	OSScreenPutFontEx(0, 0, 0, "APRIL FOOLS! With <3 (2017)");
	OSScreenPutFontEx(1, 0, 0, "APRIL FOOLS! With <3 (2017)");
	OSScreenPutFontEx(0, 0, 1, "I thought you all wanted a media player?");
	OSScreenPutFontEx(1, 0, 1, "I thought you all wanted a media player?");
	OSScreenPutFontEx(0, 0, 3, "(this was going to be an April Fools joke, but I couldn't stop myself.)");
	OSScreenPutFontEx(1, 0, 3, "(this was going to be an April Fools joke, but I couldn't stop myself.)");
	OSScreenPutFontEx(0, 0, 4, "(Enjoy.)");
	OSScreenPutFontEx(1, 0, 4, "(Enjoy.)");

	// Flush the cache
	DCFlushRange(screenBuffer, screen_buf0_size);
	DCFlushRange((screenBuffer + screen_buf0_size), screen_buf1_size);

	// Flip buffers
	OSScreenFlipBuffersEx(0);
	OSScreenFlipBuffersEx(1);

	int vpadError = -1;
    VPADData vpad;
	while(1) {
        VPADRead(0, &vpad, 1, &vpadError);

        if(vpadError == 0 && ((vpad.btns_d /*| vpad.btns_h*/) & VPAD_BUTTON_HOME))
            break;

		usleep(50000);
    }

	AXQuit();
	free(audio);
	free(video);
	MEM1_free(screenBuffer);
	screenBuffer = NULL;

    //!*******************************************************************
    //!                    Enter main application                        *
    //!*******************************************************************

    log_printf("Unmount SD\n");
    unmount_sd_fat("sd");
    log_printf("Release memory\n");
    memoryRelease();
    log_deinit();

    return EXIT_SUCCESS;
}
